
#**Beginnen met de blog**

**04-09-17**
*Het startsein voor Design Challenge 1. is gegeven. De design challenge houdt in dat er een game ontworpen moet worden voor 1e jaars studenten die door middel van de desbetreffende game kennis kunnen maken met Rotterdam. De game moet een online interactief element bezitten.*

**05-09-17**
Na een introductie te hebben gehad betreffende de eerste design challenge werd ons gevraagd een moodboard te maken met betrekking tot de doelgroep. Daarnaast werd er nog aan ons gevraagd individueel een thema te kiezen met betrekking tot Rotterdam en hier onderzoek naar te doen. 

**06-09-17**
 In overleg met ons projectgroepje werd besloten dat iedereen zijn moodboard af zou hebben voordat wij vandaag aan de les zouden beginnen. Naast het onderzoekende gedeelte begonnen wij ook al met het bedenken van een concept voor onze design challenge. 

**11-09-17**
Vandaag hebben wij als projectgroep ons concept voor de Design Challenge vastgesteld, daarbij hebben wij ons bezig gehouden met het maken van desbetreffende spelregels etc. en het maken van een paper-prototype. 

> Written with [StackEdit](https://stackedit.io/).